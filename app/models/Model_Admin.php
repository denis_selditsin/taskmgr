<?php

use Kernel\DBConnector;
use Kernel\Model;

class Model_admin implements Model
{
    public function get_data(array $post_args = null, string $get_args = null): array
    {
        $username = null;
        $password = null;
        $submit = null;
        $data = [];
        $pages = 0;
        $alert = [];
        if($post_args == null)
            $post_args = [];

        if (array_key_exists('username', $post_args))
            $username = htmlspecialchars($post_args['username']);

        if (array_key_exists('submit', $post_args))
            $submit = true;

        if (array_key_exists('password', $post_args))
            $password = htmlspecialchars($post_args['password']);

        if($username == "admin" && $password == "123")
            $_SESSION['admin'] = true;
        elseif($submit && (empty($username) or empty($password)))
            array_push($alert, "Заполните все поля");
        elseif($submit)
            array_push($alert, "Введенные данные неверны");

        if(array_key_exists('admin', $_SESSION) && $_SESSION['admin']){
            if(empty($get_args))
                $get_args = 1;
            $limit = 3;
            $limit_from = (int)$get_args * $limit - $limit;

            $count = DBConnector::query('SELECT COUNT(*) from `task`')->fetchAll()[0][0];
            $pages = ceil($count/$limit);

            if ($count < 3)
                $pages = 0;

            if(!array_key_exists('sort', $_SESSION)){
                $sort = 'ASC';
                $sort_by = 'username';
            }
            else{
                $sort = strtoupper($_SESSION['sort'][1]);
                $sort_by = $_SESSION['sort'][0];
            }


            $q = 'SELECT * from `task` ORDER BY '.$sort_by.' '.$sort.' LIMIT '.$limit_from.', '.$limit;
            $tasks = DBConnector::query($q);

            foreach ($tasks as $task) {
                array_push($data, $task);
            }
        }

        return array($data, $pages, 'alerts' => $alert);
    }

    public function logout()
    {
        session_destroy();

        return "logout";
    }

    public function edit(array $post_args = null, string $get_args = null)
    {
        if (empty($get_args)){
            header('Location: /admin');
        }
        $alerts = [];
        $prev_text = DBConnector::query('SELECT `task` from `task` WHERE `id` = '.$get_args)->fetchAll()[0][0];

        $email = null;
        $username = null;
        $text = null;
        $done = null;
        $id = null;
        $edited = 0;

        if (array_key_exists('email', $post_args))
            $email = htmlspecialchars($post_args['email']);

        if (array_key_exists('id', $post_args))
            $id = htmlspecialchars($post_args['id']);

        if (array_key_exists('done', $post_args))
            $done = htmlspecialchars($post_args['done']);

        if (array_key_exists('username', $post_args))
            $username = htmlspecialchars($post_args['username']);

        if (array_key_exists('text', $post_args)){
            $text = htmlspecialchars($post_args['text']);
            if($text!=$prev_text){
                $edited = 1;
            }
        }


        $query = null;
        if (array_key_exists('submit', $post_args)){
            if(empty($email))
                array_push($alerts, "Заполните поле Email");
            if(empty($username))
                array_push($alerts, "Заполните поле Username");
            if(empty($text))
                array_push($alerts, "Заполните поле Текст задачи");
            if(empty($alerts)){
                if($done == "on")
                    $done = 1;
                else
                    $done = 0;
                $query = DBConnector::query("UPDATE `task` SET task='$text', email='$email', username='$username', done='$done', edited='$edited' WHERE `id` = ".$get_args);
            }

        }
        $tasks = DBConnector::query('SELECT * from `task` WHERE `id` = '.$get_args);

        return array('alerts' => $alerts, $tasks->fetchAll());
    }

    public function delete(array $post_args = null, string $get_args = null)
    {
        if (empty($get_args)){
            header('Location: /admin');
        }
        $query = DBConnector::query("DELETE FROM `task` WHERE `id` = ".$get_args);

        return $get_args;
    }
}