<?php

use Kernel\Model;
use Kernel\DBConnector;

class Model_main implements Model
{

    public function get_data(array $post_args = null, string $get_args = null): array
    {
        $data = [];
        if(empty($get_args))
            $get_args = 1;
        $limit = 3;
        $limit_from = (int)$get_args * $limit - $limit;

        $count = DBConnector::query('SELECT COUNT(*) from `task`')->fetchAll()[0][0];
        $pages = ceil($count/$limit);

        if ($count < 3)
            $pages = 0;

        if(!array_key_exists('sort', $_SESSION)){
            $sort = 'ASC';
            $sort_by = 'username';
        }
        else{
            $sort = strtoupper($_SESSION['sort'][1]);
            $sort_by = $_SESSION['sort'][0];
        }


        $q = 'SELECT * from `task` ORDER BY '.$sort_by.' '.$sort.' LIMIT '.$limit_from.', '.$limit;
        $tasks = DBConnector::query($q);

        foreach ($tasks as $task) {
            array_push($data, $task);
        }


        return array($data, $pages);
    }


}