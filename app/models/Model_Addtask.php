<?php

use Kernel\DBConnector;
use Kernel\Model;

class Model_addtask implements Model
{

    public function get_data(array $post_args = null, string $get_args = null):array
    {
        $alerts = [];

        $email = null;
        $username = null;
        $text = null;
        $good = 0;

        if (array_key_exists('email', $post_args))
            $email = htmlspecialchars($post_args['email']);

        if (array_key_exists('username', $post_args))
            $username = htmlspecialchars($post_args['username']);

        if (array_key_exists('text', $post_args))
            $text = htmlspecialchars($post_args['text']);
        $query = null;
        if (array_key_exists('submit', $post_args)){
            if(empty($email))
                array_push($alerts, "Заполните поле Email");
            if(empty($username))
                array_push($alerts, "Заполните поле Username");
            if(empty($text))
                array_push($alerts, "Заполните поле Текст задачи");

            if(empty($alerts)) {
                $query = DBConnector::query("INSERT INTO `task` VALUES(0, '$text', '$email', '$username', 0, 0) ");
                $good = 1;
            }
        }

        return array('alerts' => $alerts, $query, 'good' => $good);
    }
}