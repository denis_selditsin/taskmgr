<h3>Список задач</h3>
<table class="table table-responsive">
    <thead>
    <tr>
        <th scope="col"><a href="/resort/main/username">Имя пользователя</a></th>
        <th scope="col"><a href="/resort/main/email">Email</a></th>
        <th scope="col">Текст задачи</th>
        <th scope="col"><a href="/resort/main/done+edited">Статус</a></th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($data[0] as $task) {
            echo "
            <tr>
                <td>".$task['username']."</td>
                <td>".$task['email']."</td>
                <td>".$task['task']."</td>";
                if($task['done'] == 1)
                    echo "<td><span class=\"tag tag-success\">Выполнено</span></td>";
                else
                    echo "<td><span class=\"tag tag-warning\">В процессе</span></td>";
                if($task['edited'] == 1)
                    echo "<td><span class=\"tag tag-success\">Отредактировано администратором</span></td>";
                else
                    echo "<td><span class=\"tag tag-warning\"></span></td>";
            echo "</tr>";
        }
    ?>
    </tbody>
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <?php
                for ($i = 1; $i <=$data[1]; $i++){
                    echo "<li class=\"page-item\"><a class=\"page-link\" href=\"/main/main/$i\">$i</a></li>";
                }
            ?>

        </ul>
    </nav>
</table>
