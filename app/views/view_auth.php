<?php
if(!empty($data['alerts'])) {
    echo "<div class=\"alert alert-danger\" role = \"alert\" >";
    foreach ($data['alerts'] as $alert) {
        echo "$alert<br>";
    }
    echo "</div>";
}

?>
<h3>Авторизация</h3>
<div class="col-4">
    <form action="/admin" method="post">
        <div class="form-group">
            <label for="exampleInputEmail1">Username</label>
            <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1">
        </div>
        <button type="submit" name="submit" class="btn btn-primary">Вход</button>
    </form>
</div>

