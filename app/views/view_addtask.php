<?php
if(!empty($data['alerts'])) {
    echo "<div class=\"alert alert-danger\" role = \"alert\" >";
    foreach ($data['alerts'] as $alert) {
        echo "$alert<br>";
    }
    echo "</div>";
}

if($data['good'] == 1){
    echo "<div class=\"alert alert-success\" role=\"alert\">
              Запись добавлена
            </div>";
}
?>


<h3>Добавить задачу</h3>
<form action="/addtask" method="post">
    <div class="form-group">
        <label for="exampleFormControlInput1">Email</label>
        <input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput1">Username</label>
        <input type="text" name="username" class="form-control" id="exampleFormControlInput1" placeholder="username">
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Текст задачи</label>
        <textarea class="form-control" name="text" id="exampleFormControlTextarea1" rows="3"></textarea>
    </div>
    <button type="submit" name="submit" class="btn btn-primary mb-2">Добавить</button>
</form>