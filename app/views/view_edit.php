<?php
if(!empty($data['alerts'])) {
    echo "<div class=\"alert alert-danger\" role = \"alert\" >";
    foreach ($data['alerts'] as $alert) {
        echo "$alert<br>";
    }
    echo "</div>";
}

?>
<h3>Редактировать</h3>

<?php
    $id = $data[0][0]['id'];
    $email = $data[0][0]['email'];
    $username = $data[0][0]['username'];
    $text = $data[0][0]['task'];
    $done = $data[0][0]['done'];
    echo "ID: ".$id."<br>";

    echo "<form action=\"/admin/edit/$id\" method=\"post\">
            <div class=\"form-group\">
                <label for=\"exampleFormControlInput1\">Email</label>
                <input type=\"email\" name=\"email\" class=\"form-control\" value=\"$email\" id=\"exampleFormControlInput1\" placeholder=\"name@example.com\">
            </div>
            <div class=\"form-group\">
                <label for=\"exampleFormControlInput1\">Username</label>
                <input type=\"text\" name=\"username\" class=\"form-control\" value=\"$username\" id=\"exampleFormControlInput1\" placeholder=\"username\">
            </div>
            <div class=\"form-group\">
                <label for=\"exampleFormControlTextarea1\">Текст задачи</label>
                <textarea class=\"form-control\" name=\"text\" id=\"exampleFormControlTextarea1\" rows=\"3\">$text</textarea>
            </div>
            <div class=\"form-group form-check\">";
    if($done)
        echo "        <input type=\"checkbox\" class=\"form-check-input\" id=\"exampleCheck1\" name='done' checked>";
    else
        echo "        <input type=\"checkbox\" class=\"form-check-input\" id=\"exampleCheck1\" name='done'>";

    echo"        <label class=\"form-check-label\" for=\"exampleCheck1\">Выполнено</label>
            </div>
            <button type=\"submit\" name=\"submit\" class=\"btn btn-primary mb-2\">Сохранить изменения</button>
        </form>";

