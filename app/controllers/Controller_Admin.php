<?php


use Kernel\Controller;

class Controller_admin extends Controller
{
    function __construct()
    {
        parent::__construct();
        $this->model = new Model_admin();
    }

    public function action_main(array $post_args = null, string $get_args = null)
    {
        $data = $this->model->get_data($post_args, $get_args);

        if(array_key_exists('admin', $_SESSION) && $_SESSION['admin'] == true)
            $this->view->generate('view_admin.php', 'template.php', $data);
        else
            $this->view->generate('view_auth.php', 'template.php', $data);
    }

    public function action_logout(array $post_args = null, string $get_args = null)
    {
        $data = $this->model->logout();
        $this->view->generate('view_auth.php', 'template.php', $data);
    }

    public function action_edit(array $post_args = null, string $get_args = null)
    {
        if(array_key_exists('admin', $_SESSION) && $_SESSION['admin'] == true){
            $data = $this->model->edit($post_args, $get_args);
            $this->view->generate('view_edit.php', 'template.php', $data);
        }else{
            $this->action_main();
        }

    }

    public function action_delete(array $post_args = null, string $get_args = null)
    {
        if(array_key_exists('admin', $_SESSION) && $_SESSION['admin'] == true){
            $data = $this->model->delete($post_args, $get_args);
            $this->view->generate('view_delete.php', 'template.php', $data);
        }else{
            $this->action_main();
        }
    }


}