<?php

use Kernel\Controller;
use Kernel\View;

class Controller_main extends Controller
{
    function __construct()
    {
        parent::__construct();
        $this->model = new Model_main();
    }

    public function action_main(array $post_args = null, string $get_args = null)
    {
        $data = $this->model->get_data($post_args, $get_args);
        $this->view->generate('view_main.php', 'template.php', $data);
    }

    public function action_resort(array $post_args = null, string $get_args = null){
        if(!array_key_exists('sort', $_SESSION) || $_SESSION['sort'] == 'asc')
            $_SESSION['sort'] = 'desc';
        elseif($_SESSION['sort'] == 'desc')
            $_SESSION['sort'] = 'asc';

        header('Location: /main/main/'.$get_args);
    }
}
