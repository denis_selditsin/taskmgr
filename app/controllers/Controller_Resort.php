<?php

use Kernel\Controller;

class Controller_resort extends Controller
{

    function action_main(array $post_args = null, string $get_args = null)
    {
        if(!array_key_exists('sort', $_SESSION))
            $_SESSION['sort'] = array('username', 'desc');
        elseif($_SESSION['sort'][1] == 'asc')
            $_SESSION['sort'] = array($get_args, 'desc');
        elseif($_SESSION['sort'][1] == 'desc')
            $_SESSION['sort'] = array($get_args, 'asc');

        header('Location: '.$_SERVER['HTTP_REFERER']);
    }
}