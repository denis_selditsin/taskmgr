<?php

use Kernel\Controller;
use Kernel\View;

class Controller_addtask extends Controller
{
    function __construct()
    {
        parent::__construct();
        $this->model = new Model_addtask();
    }

    public function action_main(array $post_args = null, string $get_args = null)
    {
        $data = $this->model->get_data($post_args);
        $this->view->generate('view_addtask.php', 'template.php', $data);
    }
}