<?php


namespace Kernel;

use Kernel\View;
use Kernel\Model;

abstract class Controller
{
    public Model $model;
    public View $view;

    function __construct()
    {
        $this->view = new View();
    }

    abstract function action_main(array $post_args = null, string $get_args = null);

}