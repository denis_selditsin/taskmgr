<?php


namespace Kernel;


interface Model
{
    public function get_data(array $post_args = null, string $get_args = null);
}